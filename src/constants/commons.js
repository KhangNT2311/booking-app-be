const MODEL = {
  HOME: 'Home',
  HOME_TYPE: 'HomeType',
  HOME_FEE: 'HomeFee',
  USER: 'User',
  REFRESH_TOKEN: 'RefreshToken',
  PROVINCE: 'Province',
  AMENITY: 'Amenity',
  AMENITY_TYPE: 'AmenityType',
};
const STATUS = {
  ACTIVE: 'active',
  PENDING: 'pending',
  VOID: 'void',
  INACTIVE: 'inactive'
}
const ROLE = {
  USER: 'user',
  ADMIN: 'admin',
  HOST: 'host',
};
const RULE = {
  ALL_DAYS: 0,
  WEEKDAY: 1,
  WEEKEND: 2,
  CUSTOM_DATE: 3,
};
const ROLES = Object.values(ROLE);
const RULES = Object.values(RULE);
const STATUSES = Object.values(STATUS)
module.exports = {
  MODEL,
  ROLES,
  ROLE,
  RULES,
  RULE,
  STATUSES,
  STATUS
};
