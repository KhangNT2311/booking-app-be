const express = require('express');
const userRoutes = require('./user.route');
const authRoutes = require('./auth.route');
const homeRoutes = require('./home.route');
const homeTypeRoutes = require('./homeType.route');
const provinceRoutes = require('./province.route');
const amenityRoutes = require('./amenity.route');
const amenityTypeRoutes = require('./amenityType.route');
const settingRoutes = require('./setting.route');

const router = express.Router();

/**
 * GET v1/status
 */
router.get('/status', (req, res) => res.send('OK'));

/**
 * GET v1/docs
 */
router.use('/docs', express.static('docs'));

router.use('/users', userRoutes);
router.use('/auth', authRoutes);
router.use('/homes', homeRoutes);
router.use('/homeTypes', homeTypeRoutes);
router.use('/provinces', provinceRoutes);
router.use('/amenities', amenityRoutes);
router.use('/amenityTypes', amenityTypeRoutes);
router.use('/settings', settingRoutes);

module.exports = router;
